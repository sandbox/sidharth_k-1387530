///////////////////////////////////////////////////////////////////////////////
// Setup Example
// Add the following lines in your settings.php file
// Note that sds and someothersys are arbitrary but unique session names

$conf['session_caching_client'] = array(
  'sds' => array(
    'username' => 'admin',
    'password' => 'admin',
    'base_api_url' => 'http://localhost/statefarm/htdocs/api'
  ),
  'someothersys' => array(
    'username' => 'xyz',
    'password' => 'abc',
    'base_api_url' => 'http://example.com/drupal/restapi'
  )
);



///////////////////////////////////////////////////////////////////////////////
// Example
// NOTE: Please make sure 
// (1) your login credentials are able to request below services
// (2) authentication is enabled for the drupal services module
// (3) the resource is enabled
///////////////////////////////////////////////////////////////////////////////

function somefunc_test() {
  try {
    $sess = SessionCachingClient::session('sds');
    if (!isset($sess)) {
      return;
    }
    
    $response = $sess->session_cached_http_request('http://localhost/statefarm/htdocs/test/user/1', array('Accept' => 'application/json'), 'GET');
    $response = $sess->session_cached_http_request('http://localhost/statefarm/htdocs/test/user/2', array('Accept' => 'application/json'), 'GET');

    $another = SessionCachingClient::session('someothersys');
    if (!isset($another)) {
      return;
    }
    
    $response = $another->session_cached_http_request('http://localhost/statefarm/htdocs/test/node/1', array('Accept' => 'application/json'), 'GET');
    
  } catch (SessionCachingClientException $e) {
    $msg = $e->getMessage();
    $response = $e->getResponse();
    drupal_set_message("$msg ($response->code : $response->error)");
  }
}
