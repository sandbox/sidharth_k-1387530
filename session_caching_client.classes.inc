<?php

class SessionCachingClientException extends Exception {
  // Client defined errors
  const UNSUCCESSFUL_LOGIN = 101;
  const REMOTE_ERROR = 102;
  const NOT_SETUP_PROPERLY = 103;

  protected $response;

  /**
   * Returns the http response object
   * NOTE: a http repsonse object is returned by drupal_http_request()
   */
  public function getResponse() {
    return $this->response;
  }

  /**
   * Construct a new exception
   *
   * @param $code
   *   error code
   * @param $response
   *   the http response object returned by drupal_http_request()
   */
  public function __construct($code, $response = NULL, $message = NULL) {
    // In case you want to do a detailed analysis on exception later
    if (!empty($response)) {
      $this->response = $response;
    }

    if ($code == self::UNSUCCESSFUL_LOGIN) {
      $message = t("Unsuccessful login into remote SDS system");
    }
    else if ($code == self::REMOTE_ERROR) {
      $message = t("SDS Remote error");
    }
    else if ($code == self::NOT_SETUP_PROPERLY) {
      $message = t("Configuration settings for SessionCachingClient not found under key '$response'");
    }

    if (empty($message)) {
      $message = t("Unknown error");
    }

    parent::__construct($message, $code);
  }

}

/*
 * A single instance of this class stores a persistent, logged in connection to
 * a remote Drupal REST service.
 *
 * Create new instance through the SessionCachingClient::session() static
 * factory method.
 *
 * The remote Drupal system is assumed to be running the Services module (REST
 * server).
 *
 * @author Sidharth Kshatriya <sid@lullabot.com>
 *
 */

class SessionCachingClient {

  /**
   * username to login to remote Drupal system
   * @var string
   */
  protected $username;

  /**
   * password to login to remote Drupal system
   * @var string
   */
  protected $password;

  /**
   * The REST services endpoint of the remote Drupal system
   * e.g. http://example.com/api
   *
   * $base_api_url . '/user/login' is assumed to provide REST login service
   * @var string
   */
  protected $base_api_url;

  /**
   * A unique name to identify this session
   * @var string
   */
  protected $name;

  /**
   * Stores the cookie header corresponding to this instance's persistent,
   * logged in, Drupal session in an array
   *
   * e.g. array("Cookie" =>"SESS297ecebfa892d9186f261aae5953b6b6=XDT2ntABmkJriXfJcABkQ3SjKFDu4KEXd7J1QZ-5_80")
   * @var array
   */
  protected $cookie_header = array();

  /**
   * The cid of the cookie header
   * It is "scc.cookie_header.$this->name"
   * @var string
   */
  protected $cookie_header_cache_key;

  /**
   * Stores an array of instances of the SessionCachingClient class.
   * Each instance represents a persistent, authenticated session with a remote
   * Drupal system.
   * @static
   */
  protected static $instance = array();
  protected static $std_headers = array(
    'Accept' => 'application/json',
    'Content-Type' => 'application/json'
  );
  protected static $std_accept_header = array(
    'Accept' => 'application/json',
  );

  /**
   * Constructor for SessionCachingClient. It is not meant to be used by end
   * user. Called by self::session($name)
   *
   * The function is protected because we want subclasses to be able to call it
   *
   * @param $name
   *   A *unique* name to identify a session.
   * @param $username
   *   Drupal username in remote system
   * @param $password
   *   Drupal password in remote system
   * @param $base_api_url
   *   The REST services endpoint of the remote Drupal system e.g.
   *   http://example.com/api
   *   NOTE: $base_api_url . '/user/login' is assumed to provide REST login service
   * @see
   *   self::session($name)
   * @access
   *   protected
   * @throws
   *   SessionCachingClientException (due to call to login())
   */
  protected function __construct($name, $username, $password, $base_api_url) {
    $this->username = $username;
    $this->password = $password;
    $this->base_api_url = $base_api_url;
    $this->name = $name;
    $this->cookie_header_cache_key = "scc.cookie_header.$this->name";

    $cookie_object = cache_get($this->cookie_header_cache_key);

    if ($cookie_object === 0) {
      $this->login(); // throws a SessionCachingClientException if not successful
    }
    else {
      $this->cookie_header = $cookie_object->data;
    }
  }

  /**
   * Perform an HTTP request. Correctly handle GET, POST, PUT or any other HTTP
   * requests. Handles redirects. This function wraps drupal_http_request().
   *
   * Before making a HTTP request, checks if there is any Drupal session cookie
   * associated with the "session" (uniquely identified by $this->name). If so,
   * it tacks on the cookie header. Now the request would be authenticated with
   * the username used to originally login to the system.
   *
   * If the Drupal session expired or is invalid (perhaps host cleared out the
   * sessions table?), it will transparently re-login to the remote Drupal
   * system and cache the new session cookie for future requests.
   *
   * @param $url
   *   A string containing a fully qualified URI.
   * @param $headers
   *   An array containing an HTTP header => value pair.
   * @param $method
   *   A string defining the HTTP request to use.
   * @param $data
   *   A string containing data to include in the request.
   * @param $retry
   *   An integer representing how many times to retry the request in case of a
   *   redirect.
   * @return
   *   An object containing the HTTP request headers, response code, protocol,
   *   status message, headers, data and redirect status.
   * @see
   *   drupal_http_request()
   * @throws
   *   SessionCachingClientException (due to call to login())
   */
  public function session_cached_http_request($url, $headers = array(), $method = 'GET', $data = NULL, $retry = 3) {
    // First we check if we are logged in at all
    // This check is required in a rare scenario that user caught a
    // SessionCachingClientException in the past and now is is calling
    // session_cached_http_request()
    if (empty($this->cookie_header)) {
      $this->login(); // will throw an exception if not successful
    }
    // Add cookie to the pre-existing headers
    $headers += $this->cookie_header;
    $response = drupal_http_request($url, $headers, $method, $data, $retry);

    // If login credentials have expired in the interim, re-login
    if ($response->code == 401) {
      $this->login(); // will throw an exception if not successful
      // Retry
      // After login the cookie header would have changed
      $headers += $this->cookie_header;
      $response = drupal_http_request($url, $headers, $method, $data, $retry);
    }

    return $response;
  }

  /**
   * Logs into remote Drupal system.
   *
   * Assumes the remote system is running Drupal Services module (REST server).
   * Assumes the login URL is $this->base_api_url . '/user/login'
   *
   * This function has private scope because we don't want subclasses to ever
   * call it explicitly. It works transparently.
   *
   * @return
   *   nothing
   *
   * @throws
   *   SessionCachingClientException with message "unsuccessful login" and
   *   error code as the http response code. This happens when login was
   *   unsuccessful.
   *
   * @access private
   */
  private function login() {
    $data = array(
      'username' => $this->username,
      'password' => $this->password,
    );
    $data = json_encode($data);

    // Make sure the no stale sessions cookies are around
    $this->flush_session_cookie();

    $response = drupal_http_request($this->base_api_url . '/user/login', self::$std_headers, 'POST', $data);
    // Check if login was successful
    if ($response->code == 200) {
      $data = json_decode($response->data, TRUE);
      $cookie_string = $data['session_name'] . '=' . $data['sessid'];
      $this->cookie_header = array('Cookie' => $cookie_string);

      // This function is in pecl_http. It may not be available on all
      // installations. It allows us to parse the Set-Cookie http header which
      // can be tricky. In our case, we are interested in cookie expiry.
      // If the function does not exist, cookie expiry is taken to be the Drupal
      // default of 2,000,000 s (~23 days). Not having this function available
      // is actually not too much of an issue because:
      // (1) Not many sites will mess with the default drupal cookie expiry, so
      //     our guess should be okay.
      // (2) This just determines the expiry of the cookie in drupal cache; if
      //     the login cookie turns out to be invalid, a (re) login() will fix
      //     that.
      if (function_exists('http_parse_cookie')) {
        $cookie = http_parse_cookie($response->headers['Set-Cookie']);
        if (isset($cookie->expires)) {
          $expiry = $cookie->expires;
        }
      }

      // This will happen if http_parse_cookie() was not available or the cookie
      // expiry was not available
      if (!isset($expiry)) {
        // Assume Drupal default of 2000000 seconds
        $expiry = strtotime('+2000000 s');
      }

      cache_set($this->cookie_header_cache_key, $this->cookie_header, 'cache', $expiry);
      return;
    }

    throw new SessionCachingClientException(SessionCachingClientException::UNSUCCESSFUL_LOGIN, $response);
  }

  /**
   * Creates a new, persistent, session with a remote Drupal system or re-uses
   * a previously created session. This function is the only way to get hold
   * of a SessionCachingClient object.
   *
   * @param $name
   *   A *unique* name to identify a session. $name is used to look up the
   *   configuration parameters in the session_caching_client Drupal variable.
   * @param $className (optional)
   *   If you subclass SessionCachingClient (lets say subclass is called XY),
   *   you want to create an instance of XY in the SessionCachingClient::session
   *   and NOT SessionClachingClient. This parameter allows you to state the
   *   name of the subclass. By default it is set to SessionCachingClient.
   * @return
   *   An instance of SecureCachingClient if configuration parameters for $name
   *   were found in the sesssion_caching_client variable.
   *
   *   The returned instance may be new or may be a previously cached instance
   *   depending on whether this is the first or subsequent call to
   *   SessionCachingClient::session()
   * @throws
   *   SessionCachingClientException if, while creating a new instance it was
   *   not possible to log into remote Drupal system of if the configuration
   *   variables are not setup properly.
   */
  public static function session($name, $className = __CLASS__) {
    // The objective of the code below is to never create more than one instance
    // for each $name. In a way, it is a singleton for each unique $name.
    // Before we create an instance for a $name, we check if we have created it
    // before by looking at the class variable $instance. If so, we hand that
    // out. If not, we create an instance, and cache it for future.
    if (!isset(self::$instance[$name])) {

      $config = variable_get('session_caching_client', array());
      if (!isset($config[$name])) {
        throw new SessionCachingClientException(SessionCachingClientException::NOT_SETUP_PROPERLY, $name);
      }

      $username = $config[$name]['username'];
      $password = $config[$name]['password'];
      $base_api_url = $config[$name]['base_api_url'];

      self::$instance[$name] = new $className($name, $username, $password, $base_api_url);
    }
    return self::$instance[$name];
  }

  /**
   * Clears out any stored session cookie headers in the instance and cache.
   * You should NOT need to use this function normally but it is provided as
   * a utility.
   *
   * The effect of this will be to force a login() on the next
   * session_cached_http_request()
   *
   * Similar to flush_all_session_cookies()
   *
   * Note: if you just want to flush cookies as a whole, non progammatically;
   * simply clear the drupal cache. This will delete all session cookie headers
   * stored in cache.
   *
   * @see
   *   flush_all_session_cookies()
   *
   */
  public function flush_session_cookie() {
    $this->cookie_header = array();
    cache_clear_all($this->cookie_header_cache_key, 'cache');
  }

  /**
   * Clears out all session cookie headers stored by SessionCachingClient.
   * This is a a utility function and should NOT be used generally.
   *
   * The effect of this will be to force a login() on the next
   * session_cached_http_request()
   *
   * Similar to flush_session_cookie()
   *
   * Note: if you just want to flush cookies as a whole, non progammatically;
   * simply clear the drupal cache. This will delete all session cookie headers
   * stored in cache.
   *
   * @see
   *   flush_session_cookie()
   */
  public function flush_all_session_cookies() {
    $this->cookie_header = array();
    cache_clear_all('scc.cookie_header.*', 'cache', TRUE);
  }

}

